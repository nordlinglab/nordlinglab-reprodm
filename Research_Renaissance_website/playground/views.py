from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import pybliometrics_ml.generate
import datetime
import json

today = datetime.datetime.today()
print(today.ctime())  # Tue Oct 19 00:00:00 2021
time = today.strftime('%Y.%m.%d.%H.%M.%S')  # 2021.10.19


def home_page(request):

    return render(request, 'home_page.html', {"time": time})


def all_articles(request):

    global message, year1, year2
    if request.method == 'POST':
        message = request.POST.get('message')
        year1 = request.POST.get('year1')
        year2 = request.POST.get('year2')
        pybliometrics_ml.generate.find_article(message, int(year1), int(year2))
    file_path = 'pybliometrics_ml/output/merged_data/merged_file.json'

    with open(file_path, 'r', encoding='utf-8') as f:
        article_list = json.load(f)

    context = {
        "articleList": article_list,
        "my_string": message,
        "year": f'from {year1} to {year2}'
    }
    return render(request, 'list_paper.html', context)


def submit_message(request):
    if request.method == 'POST':
        message = request.POST.get('message')
        # 現在你可以使用這個 message 變量來進行進一步的處理
        return HttpResponse(f"Received message: {message}")
    else:
        return render(request, 'input_form.html')


def test(request):
    if request.method == 'POST':
        message = request.POST.get('message')
        year1 = request.POST.get('year1')
        year2 = request.POST.get('year2')
        return HttpResponse(f'搜尋字串：{message}<br>選擇的年份1是：{year1}<br>選擇的年份2是：{year2}')
    return HttpResponse("請使用POST方法提交數據")

import requests
import json

def fetch_citation_count(api_key, scopus_id):
    # Citation Count API的URL端點
    url = f"https://api.elsevier.com/content/abstract/citation-count?scopus_id={scopus_id}"

    # API請求頭部，包含API密鑰
    headers = {
        "X-ELS-APIKey": api_key,
        "Accept": "application/json"
    }

    # 發送GET請求
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        data = response.json()
        # 返回引用數
        return data['citation-count-response']['citation-count']
    else:
        print(f"Error fetching citation count: {response.status_code}")
        return "No citation data available"

def fetch_all_abstracts(api_key, query, max_results=100):
    # Scopus API 的 URL 端點
    url = "https://api.elsevier.com/content/search/scopus"

    # API請求頭部，包含API密鑰
    headers = {
        "X-ELS-APIKey": api_key,
        "Accept": "application/json"
    }

    # 初始化結果列表
    abstracts = []

    # 由於API限制，每次最多返回25條，需要多次請求來獲取更多數據
    for start in range(0, max_results, 25):
        params = {
            "query": query,
            "start": start,
            "count": 25  # 每次查詢限制為25篇文章
        }

        # 發送GET請求
        response = requests.get(url, headers=headers, params=params)
        if response.status_code == 200:
            data = response.json()
            # 提取每篇文章的摘要和引用數
            for entry in data['search-results']['entry']:
                scopus_id = entry.get('dc:identifier', '').split(':')[-1]
                citation_count = fetch_citation_count(api_key, scopus_id)
                abstract = entry.get('dc:description', 'No abstract available')
                abstracts.append({
                    "title": entry.get('dc:title', 'No title available'),
                    "abstract": abstract,
                    "citation_count": citation_count
                })
        else:
            print("Error: " + str(response.status_code))
            break  # 如果發生錯誤，停止循環

    # 保存結果到JSON文件
    with open('scopus_abstracts.json', 'w', encoding='utf-8') as f:
        json.dump(abstracts, f, ensure_ascii=False, indent=4)

    return "Results saved to scopus_abstracts.json"

# 使用範例
api_key = "98dadc30eaf5c1b10ab05927b2ec99ca"  # 替換為你的Scopus API密鑰
query = "TITLE-ABS-KEY(artificial intelligence)" # 替換為你的查詢條件
result = fetch_all_abstracts(api_key, query, max_results=100)  # 設定最大結果數為100篇文章


print(result)

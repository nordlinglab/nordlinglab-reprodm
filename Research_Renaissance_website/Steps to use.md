

Research Renaissance Team Spring 2024


########################

Web Service Documentation for Paper Analysis




Project

Scientific Information Gathering and Processing for Engineering Research


Spring Semester 2024


Research Renaissance


###############

Joey Wang,


Prashant Kishor Sharma,

Henry &

Sibo

###############




Here are the steps for using the web service:


---

Using the Web Service


Step 1: Retrieving the Search String from Scopus

1. Perform a Search:
   
   - Log in to your Scopus account.
   
   - Enter the keywords related to your research topic in the search bar.
   
   - Click on the search button.

2. Refine the Search:
   
   - Use Scopus' filtering options to refine your search if needed (e.g., by document type, subject area, etc.).
   
   - Review the search results to ensure they align with your research needs.

3. Copy the Search String:
   
   - Locate the search string displayed at the top of the results page.
   
   - Copy the entire search string to your clipboard.

Step 2: Accessing the Web Service

1. Open the Web Service:
   
   - Launch your web browser.
   
   - Enter the URL of the web service in the address bar and press enter.
   
   - The main page of the web service should load.

2. Log In:
   
   - If the web service requires authentication, log in using your credentials.

Step 3: Inputting the Search String

1. Locate the Search Input Field:
   
   - On the main page of the web service, find the input field designated for entering the search string.

2. Paste the Search String:
   
   - Click on the input field.
   
   - Paste the search string copied from Scopus into this field.

Step 4: Filtering by Year

1. Access Filtering Options:
   
   - Look for the filtering options on the web service interface.
   
   - These options are typically located near the search input field or in a sidebar.

2. Set the Year Filter:
   
   - Select the year or range of years you want to filter the articles by.
   
   - Apply the filter by clicking on the appropriate button or pressing enter.

Step 5: Retrieving and Viewing Articles

1. Process the Search:
   
   - After inputting the search string and setting the filters, initiate the search process by clicking on the search button.

2. View the Results:
   
   - The web service will display a list of articles matching your search criteria.
   
   - Each article entry will include the number of articles and their abstract details.


writen by Joey
testetstetaedwduahwdakwudha k,wduhwd,au whd,awudha, wduhw,d uawh,

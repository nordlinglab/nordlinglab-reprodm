

Research Renaissance Team Spring 2024


########################

Web Service Documentation for Paper Analysis




Project

Scientific Information Gathering and Processing for Engineering Research


Spring Semester 2024


Research Renaissance


###############

Joey Wang,
Prashant Kishor Sharma,
Henry &
Sibo

###############


Table of Contents



1.	Introduction
2.	Getting Started
  	  - go to https://dev.elsevier.com/ for getting a Scopus API key
3.	Using the Web Service
    - Step 1: Retrieving the Search String from Scopus
    - Step 2: Accessing the Web Service
    - Step 3: Inputting the Search String
    - Step 4: Filtering by Year
    - Step 5: Retrieving and Viewing Articles
4.	Features
    - API input
    - Search String Input
    - Year Filtering
    - Article Count
    - Abstract Details
5.	Example Usage
    - Scenario 1: Broad Search
    - Scenario 2: Narrowed Search by Year
6.	Troubleshooting
    - The search speed of the website depends on your search string. Try to avoid using overly broad search strings.
    - Everytime you submit a new search string, it may take a while to run.
    - "pybliometrics.scopus.exception.Scopus400Error: Error translating query" means that you use a wrong search string, make sure you copy a correct one.
    - If you haven't put your 
    - For non-subscribers, if the number of search results exceeds 5000 will show erorrs.
7.	Requirement package
    - pybliometrics
    - json
    - tqdm
    - shutil
    - os
8.	FAQs
    - Q: How to input the API key?
    - A: You need to go to the IDE and paste it in terminal
9.	Appendices
    - Appendix A: Glossary
    - Appendix B: References








Introduction

This documentation provides detailed guidance for using our web service designed for academic paper analysis. 
The web service aims to streamline the process of gathering, filtering, and reviewing academic papers from Scopus, a comprehensive abstract and citation database for scholarly research.

Purpose of the Web Service

Academic researchers often face challenges in efficiently locating and analyzing relevant research papers. 
This web service addresses these challenges by offering a user-friendly interface that integrates seamlessly with Scopus, allowing users to perform advanced searches, apply specific filters, and quickly access detailed information about research articles. 
The key objectives of the web service include:

- Enhancing Research Efficiency: By automating the search and filtering process, the web service saves researchers time and effort, allowing them to focus on the content rather than the mechanics of the search.
- Improving Accuracy: The web service ensures that search results are accurate and comprehensive by leveraging the robust database of Scopus.
- Facilitating In-Depth Analysis: With features such as detailed abstracts and article counts, the web service helps researchers quickly assess the relevance of articles to their specific research questions.

Scope of the Documentation

This documentation provides a comprehensive overview of how to use the web service, from initial setup to advanced usage scenarios. It covers the following areas:

1. Getting Started: Information on prerequisites and account setup to ensure users are prepared to use the web service.

2. Using the Web Service: Step-by-step instructions for retrieving search strings from Scopus, accessing the web service, inputting search strings, filtering results, and viewing articles.

3. Features: Detailed descriptions of the web service features, including search string input, year filtering, article count, and abstract details.

4. Example Usage: Practical examples of how to use the web service for both broad and narrowed searches.

5. Troubleshooting: Solutions to common issues that users might encounter while using the web service.

6. Support: Information on how to contact support for further assistance.

7. FAQs: Answers to frequently asked questions to help users quickly resolve common queries.

8. Appendices: Additional resources such as a glossary of terms and references to further aid users.


Benefits of Using the Web Service


Researchers using this web service can expect several key benefits:

- Streamlined Research Process: By integrating with Scopus, the web service simplifies the process of locating and filtering research papers, making it easier for researchers to find relevant information quickly.

- Comprehensive Search Capabilities: Users can perform detailed searches using specific keywords and apply various filters to narrow down results to the most relevant articles.

- Time-Saving Features: The ability to view abstracts and article counts directly within the web service helps researchers quickly determine the relevance of search results, saving time that would otherwise be spent sifting through less pertinent articles.

- User-Friendly Interface: The intuitive design of the web service ensures that users can navigate and use the platform with minimal learning curve, making it accessible to researchers at all levels of technical expertise.

In end, this documentation serves as a complete guide for utilising the web service effectively, ensuring that researchers can maximise their productivity and accuracy in academic paper analysis. 
By following the instructions and leveraging the features outlined in this guide, users can enhance their research capabilities and achieve more efficient and effective results in their scholarly endeavours.


Getting Started

Prerequisites

Before using the web service, ensure the following prerequisites are met:

- Scopus Account: A valid account with Scopus.
- Computer with Internet: Access to a computer with a reliable internet connection.
- Web Browser: A web browser (such as Google Chrome, Mozilla Firefox, or Microsoft Edge) installed on your computer.

Account Setup

1. Create a Scopus Account:
   - Visit the Scopus website and create an account if you do not already have one.
   - Follow the instructions provided by Scopus to verify and activate your account.

2. Log In to Your Account:
   - Use your credentials to log in to the Scopus platform.

Using the Web Service

Step 1: Retrieving the Search String from Scopus

To begin the paper analysis, you need to retrieve the search string from Scopus. Follow these steps:

1.	Perform a Search:
   - Log in to your Scopus account.
   - In the search bar, enter the keywords related to your research topic (e.g., "machine learning in mechanical engineering").
   - Click on the search button.

2.	Refine the Search:
   - Use Scopus' filtering options to refine your search if needed (e.g., by document type, subject area, etc.).
   - Review the search results to ensure they align with your research needs.

3.	Copy the Search String:
   - Once you are satisfied with the search results, locate the search string (typically displayed at the top of the results page).
   - Copy the entire search string to your clipboard.

Example of Retrieving Search String

Imagine you are researching the use of artificial intelligence in healthcare. You would:

- Log in to Scopus.
- Enter "artificial intelligence in healthcare" in the search bar.
- Click on "Search."
- Refine the search by selecting relevant document types, such as articles or conference papers.
- Copy the search string displayed at the top of the results page.

Step 2: Accessing the Web Service

1.	Open the Web Service:
   - Launch your web browser.
   - Enter the URL of the web service in the address bar and press enter.
   - The main page of the web service should load.

2.	Log In:
  	 - If the web service requires authentication, log in using your credentials.
Example of Accessing the Web Service

To access the web service:

- Open Google Chrome/ Browser.
- Type in the URL (e.g., https://www.nordlinglab.org/services/ ) and press enter.
- If prompted, log in using your username and password.

Step 3: Inputting the Search String

1.	Locate the Search Input Field:
  	 - On the main page of the web service, find the input field designated for entering the search string.

2.	Paste the Search String:
  	 - Click on the input field.
  	 - Paste the search string copied from Scopus into this field.

Example of Inputting the Search String

Once on the main page:

- Locate the input field, often marked as "Search."
- Click inside the field to activate it.
- Press `Ctrl + V` (or `Cmd + V` on Mac) to paste the search string.

Step 4: Filtering by Year

1.	Access Filtering Options:
   - Look for the filtering options on the web service interface.
   - Typically, these options are located near the search input field or in a sidebar.

2.	Set the Year Filter:
  	 - Select the year or range of years you want to filter the articles by.
   	- Apply the filter by clicking on the appropriate button or pressing enter.

Example of Filtering by Year

To filter articles published from 2020 to 2023:

- Locate the filtering options.
- Select the start year (2020) and end year (2023).
- Click on "Apply Filter."

Step 5: Retrieving and Viewing Articles

1.	Process the Search:
- After inputting the search string and setting the filters, initiate the search process by clicking on the search button.

2.	View the Results:
   - The web service will display a list of articles matching your search criteria.
   - Each article entry will include the number of articles and their abstract details.

Example of Retrieving and Viewing Articles

To retrieve and view articles:

- Click on the search button after setting filters.
- The web service displays the results.
- Browse through the list to see articles with their abstracts.

Features

Search String Input

The search string input feature allows users to paste the search string obtained from Scopus into the web service. This facilitates seamless integration between Scopus and the web service.

How to Use:

- Copy the search string from Scopus.
- Paste it into the input field on the web service.

Year Filtering

Users can filter the search results by specifying a particular year or a range of years. This helps in narrowing down the results to the most relevant publications.

How to Use:

- Access the filtering options.
- Set the desired year or range of years.
- Apply the filter.

Article Count

The web service provides the total number of articles that match the search criteria. This helps users gauge the volume of research available on a specific topic.

How to Use:

- After initiating the search, view the displayed number of articles.

Abstract Details

Each article entry includes an abstract, providing a brief summary of the research. This enables users to quickly assess the relevance of each article.

How to Use:

- Browse the search results.
- Click on an article to view its abstract.

Example Usage

Scenario 1: Broad Search

1.	Retrieve Search String:
   - Search for "artificial intelligence in healthcare" on Scopus.
   - Copy the search string.

2.	Input and Filter:
   - Paste the search string into the web service.
   - Apply no additional filters.

3.	View Results:
  	 - The web service displays all articles related to artificial intelligence in healthcare.

Scenario 2: Narrowed Search by Year

1.	Retrieve Search String:
   - Search for "renewable energy sources" on Scopus.
   - Copy the search string.

2.	Input and Filter:
   - Paste the search string into the web service.
   - Filter by the years 2020 to 2023.

3.	View Results:
- The web service displays articles related to renewable energy sources published between 2020 and 2023.



Troubleshooting

Common Issues and Solutions

Issue 1: The web service does not load.

Solution:

1. Check Internet Connection:
   
   - Ensure that your device is connected to the internet. Try loading another website to verify the connection.
   
   - If other websites are also not loading, troubleshoot your internet connection (e.g., restart your router, check network cables, etc.)
   
   
2. Update Browser:
   
   - Make sure your web browser is updated to the latest version. Outdated browsers may not support the web service.
   
   - To update your browser, go to the browser's settings or help menu and check for updates.

3. Clear Cache and Cookies:
   
   - Clear your browser’s cache and cookies to ensure no old data is causing issues.
   
   - In most browsers, you can find this option in the settings menu under "Privacy" or "History."

4. Disable Browser Extensions:
   
   
  - Some browser extensions may interfere with the web service. Try disabling extensions and reloading the page.
  
   - To disable extensions, go to the extensions menu in your browser settings and toggle off each extension.

5. Try a Different Browser:
  
   - If the web service still does not load, try accessing it using a different web browser (e.g., switch from Google Chrome to Mozilla Firefox).

Issue 2: The search string is not accepted.

Solution:

1. Verify Search String:

   - Ensure the search string is correctly copied from Scopus. Verify that there are no extra spaces, special characters, or incorrect syntax.

   - Re-copy the search string directly from the Scopus search results page and paste it into the web service.

2. Check for Special Characters:

   - Remove any unnecessary special characters that might have been included in the search string. Only use characters that are part of the standard search syntax.

3. Reformat the Search String:

   - If the search string is complex, try breaking it down into simpler components to identify any issues. Reformat the string as needed.

Issue 3: No results are displayed.

Solution:

1. Broaden Filters:

   - Remove or broaden the year filters to include more articles. Sometimes too narrow filters can result in no matching articles.

   - Set a wider range of years or remove other filters temporarily to see if results appear.

2. Validate Search String:

   - Ensure the search string matches the format expected by the web service. Double-check the keywords and logical operators (AND, OR, NOT) used in the search string.

3. Check Search Criteria:

   - Make sure the search criteria are relevant to your research topic. Verify that the keywords used are likely to yield results in the database.

4. Test with Different Keywords:

   - Try using different but related keywords to see if any results are displayed. This can help identify if the issue is with specific keywords.

Detailed Troubleshooting Steps

Web Service Not Loading

1. Check Internet Connection:

   - Open a different website to ensure your internet connection is active.

   - Restart your router if the internet connection is unstable.

2. Update Browser:

   - Go to your browser’s settings or help menu.

   - Select "Check for updates" and install any available updates.

   - Restart your browser after updating.
   
3. Clear Cache and Cookies:

   - In Google Chrome: Go to "Settings" > "Privacy and security" > "Clear browsing data."

   - In Mozilla Firefox: Go to "Options" > "Privacy & Security" > "Cookies and Site Data" > "Clear Data."

   - In Microsoft Edge: Go to "Settings" > "Privacy, search, and services" > "Clear browsing data."

4. Disable Browser Extensions:

   - In Google Chrome: Go to "More tools" > "Extensions."

   - In Mozilla Firefox: Go to "Add-ons" > "Extensions."

   - In Microsoft Edge: Go to "Extensions."

5. Try a Different Browser:

   - Download and install another web browser.

   - Open the web service URL in the new browser.


Search String Not Accepted

1. Verify Search String:

   - Go back to the Scopus search results page.

   - Copy the search string again and ensure it is complete and correct.

2. Check for Special Characters:

   - Review the search string and remove any extraneous special characters.

3. Reformat the Search String:

   - Simplify the search string into smaller parts and test each part individually.

No Results Displayed

1. Broaden Filters:

   - Remove year filters and other restrictive criteria.

   - Set the start and end years to include a broader range.

2. Validate Search String:

   - Ensure logical operators are used correctly (e.g., "machine learning" AND "mechanical engineering").

3. Check Search Criteria:

   - Review the keywords and ensure they are specific yet likely to yield results.

4. Test with Different Keywords:

   - Experiment with synonyms or related terms to expand the search.

Contacting Support

If you encounter issues that are not resolved through the troubleshooting steps, please contact our support team:


- Email: [support@example.com](mailto:support@example.com)
- Phone: +1-800-123-4567
- Live Chat: Available on the web service during business hours.

Contacting Support

When contacting support, please provide the following information to help us assist you more efficiently:

- Description of the Issue: Clearly describe the problem you are experiencing.
- Steps Taken: List the steps you have already taken to try and resolve the issue.
- Screenshots: If possible, include screenshots of the error or issue.
- Browser Information: Specify the browser and version you are using.

By following these detailed troubleshooting steps, users can effectively resolve common issues and ensure a smooth experience with the web service.


FAQs

How do I retrieve a search string from Scopus?

- Log In: Log in to your Scopus account.
- Search: Enter your keywords in the search bar and click "Search."
- Copy Search String: Copy the generated search string from the top of the results page.

Can I filter articles by multiple criteria?

- Currently, the web service supports filtering by year. Additional filtering options may be available in future updates.

What should I do if the web service is down?

- Check Internet and Browser: Ensure your internet connection is active and your browser is updated.
- Contact Support: If the issue persists, contact our support team.

Is there a limit to the number of articles that can be displayed?

- Server Capacity: The web service may have a limit based on server capacity. If you encounter issues, try refining your search criteria.

How do I view abstracts of articles?

- Search Results: After performing a search, browse the list of articles.
- Click on Article: Click on an article entry to view its abstract.

Appendices

Appendix A: Glossary

- Scopus: A comprehensive abstract and citation database for academic research.
- Search String: A query used to search for articles in a database.
- Abstract: A brief summary of a research article.

Appendix B: References

- [Scopus] (https://www.scopus.com)
- [Web Service URL] (https://www.nordlinglab.org/services/ )

Appendix C: Detailed Example Walkthrough

Example: Searching for AI in Healthcare (2020-2023)

1. Log In to Scopus:
   - Visit the Scopus website and log in with your credentials.

2. Perform Search:
   - Enter "artificial intelligence in healthcare" in the search bar and click "Search."

3. Refine Search:
   - Use filters to refine results, if needed.

4. Copy Search String:
   - Copy the search string displayed at the top.

5. Access Web Service:
   - Open your browser and navigate to the web service URL.
   - Log in if required.

6. Input Search String:
   - Paste the search string into the search input field.

7. Set Year Filter:
   - Select 2020 to 2023 in the year filter options.
   - Click "Apply Filter."

8. Retrieve Articles:
   - Click on the search button.
   - View the list of articles matching the criteria, including abstracts.


This detailed documentation aims to provide users with all necessary information to effectively utilize the web service for paper analysis. Follow the steps and guidelines to streamline your research process.



# SciNest: Reproducibility Assessment Tool of Scientific Articles

## Description
SciNest is developed by four graduate students from NCKU in Professor Nordling's course "Scientific Information & Gathering (Sci Info)". 
The team members include:
- David Hernandez
- Rex Lee
- Louis 
- Dieter

Our tool allows users to upload or input the full text of scientific articles and returns a reproducibility score. 
This is powered by using the Llama3 model via Ollama.

# File Structure
**Project Directory**
```
SciNest_website
│   README.md 
│   requirements.txt  # Contains all dependencies required for the project
│   utils.py # llama3 function
│   reprod_score.py # main file
│
└───templates
│   │   response.html # front end of the website
```

## Installation

### Prerequisites
- Python 3.10 +
- Conda (recommended for environment management)
- Ollama

### Setting up the environment
1. Install Conda, if not already installed, from [Miniconda](https://docs.conda.io/en/latest/miniconda.html).

2. Create a new Conda environment in :
- If on **Windows**, open "*Anaconda Prompt*".
- If on **Linux** or **Mac**, open "*Terminal*"

   ```bash
   conda create -n your_env_name python=3.11
   ```
3. Activate the environment:
   ```bash
   conda activate your_env_name
   ```

### Setting up Ollama
1. Install Ollama, if not already installed, from [Ollama](https://ollama.com/download).
2. Download Llama3 by running:
   ```bash
   ollama run llama3
   ```

### Installing dependences
- If on **Windows**, open "*Anaconda Prompt*".
- If on **Linux** or **Mac**, open "*Terminal*"

Ensure you are in the correct:
- project directory by running:
   ```bash
   cd path/to/your/project/directory
   ```
- conda environment by running:
   ```bash
   conda activate your_env_name
   ```


Then run:
   ```bash
   pip install -r requirements.txt
   ```

# Usage
To start the application, run:
   ```bash
   python reprod_score.py
   ```
After running the script, open a web browser and navigate to:
   ```bash
   http://127.0.0.1:5000
   ```


# License
This project is licensed under Apache License 2.0

# Contact
For further assistance, contact:
- David Hernandez @ david.hernandez@nordlinglab.org
- Rex Lee @ rex.lee@nordlinglab.org
- Louis @ 
- Dieter @



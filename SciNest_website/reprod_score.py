from flask import Flask, request, render_template
import PyPDF2
import io
from utils import analysis  # Ensure this imports your analysis function
import markdown

app = Flask(__name__)

prompt = '''You are tasked with assessing the reproducibility of a research paper. Please follow these detailed steps to evaluate and score the paper's reproducibility based on specific criteria.
User Accessibility (0-10):
Is the paper written in clear, accessible language?
Is the structure of the paper logical and easy to follow?
Are technical terms well-defined or explained?
Are the fundamental equations and concepts explained properly?
Location of Data (0-10):
Does the paper specify where the data can be accessed?
Are there links to data repositories or supplementary materials?
Is the data publicly available and easily accessible?
Code Availability (0-10):
Is the code publicly available?
Data Description (0-20):
Is there a detailed description of the dataset?
What is the size and scope of the data?
Are the variables and data collection methods well-documented?
Is the preprocessing steps or transformations clearly explained?
Validation Processes for Data (0-10):
Are there any methods described to validate the data?
Is cross-validation or comparison with other datasets mentioned?
Are error checking or data cleaning procedures detailed?
Research Type (Hypothesis-Driven/Generating) (0-5):
Does the research start with a specific hypothesis or is it exploratory?
How clearly is the research question or hypothesis stated?
Reproducibility of Results (0-10):
Are the methods described in sufficient detail to be replicated?
Is the code or software used in the analysis provided or referenced?
Are the steps in the analysis clearly outlined and justified?
Detailed Analysis Sections:
Abstract:
Summarize the main objectives, methods, and findings of the paper.
Note any mention of data sources, validation, and reproducibility efforts.
Methodology:
Provide a thorough review of the research methods.
Assess the clarity and detail of the description.
Evaluate the potential for these methods to be replicated by others.
Results:
Examine how the results are presented and discussed.
Look for statistical validation and logical consistency.
Assess if the results follow clearly from the methods used.
Overall Reproducibility Score:
Based on the criteria above, calculate an overall reproducibility score.
Provide a probability percentage indicating the likelihood of the research being reproducible.
Be objective and penalize extremely if the paper fails to meet the criteria in any section
''' 

@app.route('/', methods=['GET'])
def home():
    return render_template('response.html', analysis_result='', page='home')

@app.route('/score', methods=['GET'])
def score():
    return render_template('response.html', analysis_result='', page='score')

# @app.route('/help', methods=['GET'])
# def help():
#     return render_template('response.html', analysis_result='', page='help')

@app.route('/upload', methods=['POST'])
def upload_file():
    analysis_result = ''
    text_input = request.form.get('text_input')
    file_input = request.files.get('file_input')
    
    if text_input:
        # Analyze the provided text input
        analysis_result = analysis('llama3', text_input, prompt)
    elif file_input and file_input.filename.endswith(".pdf"):
        # Analyze the provided PDF file
        pdf_reader = PyPDF2.PdfReader(io.BytesIO(file_input.read()))
        text = ""
        for page_num in range(len(pdf_reader.pages)):
            page = pdf_reader.pages[page_num]
            text += page.extract_text()
        analysis_result = analysis('llama3', text, prompt)
    
    # Convert Markdown to HTML
    analysis_result_html = markdown.markdown(analysis_result, extensions=['extra'])
    return render_template('response.html', analysis_result=analysis_result_html, page='score')

if __name__ == '__main__':
    app.run(debug=True)
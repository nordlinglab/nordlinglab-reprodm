

Welcome to our website, developed by SciNest, a team of Mechanical Engineering students from National Cheng Kung University.  We are committed to advancing scientific reproducibility by seamlessly integrating cutting-edge technologies such as the LLaMA AI generator into our user-friendly tools.
from flask import Flask, render_template, request, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user
from flask_wtf import FlaskForm
from wtforms import TextAreaField
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import InputRequired, Length, ValidationError
from werkzeug.security import generate_password_hash, check_password_hash
import PyPDF2
import io

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SECRET_KEY'] = 'thisisasecretkey'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    password = db.Column(db.String(80), nullable=False)

class ReproducibilityForm(FlaskForm):
    score = StringField('Score', validators=[InputRequired(), Length(min=1, max=10)], render_kw={"placeholder": "Enter a score from 1 to 10"})
    comments = TextAreaField('Comments', validators=[Length(max=200)], render_kw={"placeholder": "Add any comments here"})
    submit = SubmitField('Submit Score')
    
with app.app_context():
    db.create_all()  # Create database tables for all models

class Paper(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text, nullable=False)



class RegisterForm(FlaskForm):
    username = StringField(validators=[InputRequired(), Length(min=4, max=20)], render_kw={"placeholder": "Username"})
    password = PasswordField(validators=[InputRequired(), Length(min=4)], render_kw={"placeholder": "Password"})
    submit = SubmitField("Register")

    def validate_username(self, username):
        existing_user_username = User.query.filter_by(username=username.data).first()
        if existing_user_username:
            raise ValidationError("That username already exists. Please choose a different one.")

class LoginForm(FlaskForm):
    username = StringField(validators=[InputRequired(), Length(min=4, max=20)], render_kw={"placeholder": "Username"})
    password = PasswordField(validators=[InputRequired(), Length(min=4)], render_kw={"placeholder": "Password"})
    submit = SubmitField("Login")

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                return redirect(url_for('dashboard'))
            else:
                flash('Invalid username or password')
        else:
            flash('Invalid username or password')
    return render_template('login.html', form=form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        hashed_password = generate_password_hash(form.password.data, method='sha256')
        new_user = User(username=form.username.data, password=hashed_password)
        db.create_all()
        db.session.add(new_user)
        db.session.commit()
        return redirect(url_for('login'))
    return render_template('register.html', form=form)

@app.route('/dashboard')
@login_required
def dashboard():
    return render_template('dashboard.html')

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))

# @app.route('/attachment-upload', methods=["GET", "POST"])
# def attachment_upload():
#     if request.method == "POST":
#         pdf_file = request.files.get("file")  # 使用 get 避免 KeyError
#         if pdf_file and pdf_file.filename.endswith(".pdf"):
#             pdf_reader = PyPDF2.PdfReader(io.BytesIO(pdf_file.read()))
#             text = ""
#             for page_num in range(len(pdf_reader.pages)):
#                 page = pdf_reader.pages[page_num]
#                 text += page.extract_text()
#             return render_template("attachment-upload.html", text=text)

#     # 對於 GET 請求和沒有有效 PDF 文件的 POST 請求
#     return """
#     <form method='post' enctype='multipart/form-data'>
#     <input type='file' name='file'>
#     <input type='submit' value='Upload'>
#     </form>
#     """
@app.route('/attachment-upload', methods=["GET", "POST"])
def attachment_upload():
    if request.method == "POST":
        pdf_file = request.files.get("file")  # Use get to avoid KeyError
        if pdf_file and pdf_file.filename.endswith(".pdf"):
            pdf_reader = PyPDF2.PdfReader(io.BytesIO(pdf_file.read()))
            text = ""
            for page_num in range(len(pdf_reader.pages)):
                page = pdf_reader.pages[page_num]
                text += page.extract_text()
            flash('File processed successfully.')
            return render_template("dashboard.html", text=text)  # You can adjust this to redirect or render as needed
        else:
            flash('Please upload a valid PDF file.')
            return redirect(url_for('attachment_upload'))

    # For GET request and POST requests without a valid PDF file
    return render_template('dashboard.html')  # Assuming the upload form is part of the dashboard


@app.route('/reproducibility-score', methods=['GET', 'POST'])
def reproducibility_score():
    form = ReproducibilityForm()
    if form.validate_on_submit():
        # Here, you can process the form data and store it in the database or do something else with it
        flash('Score and comments submitted successfully!')
        return redirect(url_for('reproducibility_score'))
    return render_template('text_bottom', form=form)


@app.route('/modify-section')
def modify_section():
    return render_template('modify_section.html')

@app.route('/llms-response')
def llms_response():
    return render_template('llms_response.html')

@app.route('/submit-paper', methods=['POST'])
def submit_paper():
    paper_content = request.form['paperContent']
    # Here you can handle saving the paper content to the database or perform other operations
    flash('Paper submitted successfully!')
    return redirect(url_for('home'))



if __name__ == '__main__':
    app.run(debug=True)

import numpy as np


def git_score_caculating(n_commits, lines_inserted, lines_deleted, words_inserted, words_deleted):
    n_commits = np.log10(n_commits / 15) - 10
    lines_inserted = np.log10(lines_inserted / 300) - 10
    lines_deleted = np.log10(lines_deleted / 50) - 10
    words_inserted = np.log10(words_inserted / 1500) - 10
    words_deleted = np.log10(words_deleted / 250) - 10

    return n_commits, lines_inserted, lines_deleted, words_inserted, words_deleted

n_commits=15
lines_inserted=500
lines_deleted=500
words_inserted=500
words_deleted=500
n_commits, lines_inserted, lines_deleted, words_inserted, words_deleted = git_score_caculating(n_commits, lines_inserted, lines_deleted, words_inserted, words_deleted)
# git_score=author.n_commits*0.3 + author.summary.lines_inserted*0.2 + author.summary.lines_deleted*0.15+author.summary.words_inserted*0.2+author.summary.words_deleted*0.15
print([n_commits, lines_inserted, lines_deleted, words_inserted, words_deleted])

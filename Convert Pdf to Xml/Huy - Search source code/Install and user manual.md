# pdftotext
* Install Anaconda
* Install poppler

        conda install -c conda-forge poppler
* Install pdftotext (Anaconda Prompt)

        pip install pdftotext
# pdftableuser
* Sign up in pdftables.com and get api key

        pip install git+https://github.com/pdftables/     python-pdftables-api.git
        
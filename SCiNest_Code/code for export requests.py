import requests
from bs4 import BeautifulSoup

# URL from the user's search query on arXiv
url = "https://arxiv.org/search/advanced?advanced=&terms-0-operator=AND&terms-0-term=%22reproducibility%22+OR+%22repeatability%22+OR+%22replication%22&terms-0-field=title&terms-1-operator=AND&terms-1-term=%22tools%22+OR+%22methods%22+OR+%22assessment%22+OR+%22evaluation%22+OR+%22prediction%22+OR+%22market%22+OR+%22researcher%22+OR+%22models%22&terms-1-field=all&terms-2-operator=AND&terms-2-term=%22estimating%22+OR+%22estimation%22+OR+%22scientific+research%22+OR+%22social%22&terms-2-field=all&terms-3-operator=AND&terms-3-term=&terms-3-field=all&classification-physics_archives=all&classification-include_cross_list=include&date-filter_by=all_dates&date-year=&date-from_date=&date-to_date=&date-date_type=submitted_date&abstracts=show&size=200&order=-announced_date_first&start=400"

# Send HTTP GET request
response = requests.get(url)
response.raise_for_status()  # Check that the request was successful

# Parse the HTML content
soup = BeautifulSoup(response.text, 'html.parser')

# Find all article entries on the page
articles = soup.find_all('li', class_='arxiv-result')

# Iterate through each article and extract information
data = []
for article in articles:
    title = article.find('p', class_='title').get_text(strip=True)
    authors = [author.get_text(strip=True) for author in article.find_all('a', class_='author')]
    abstract = article.find('p', class_='abstract').get_text(strip=True).replace('Abstract:', '').strip()
    
    # Append data to the list
    data.append({
        'Title': title,
        'Authors': authors,
        'Abstract': abstract
    })

# Optionally print the data
for item in data:
    print("Title:", item['Title'])
    print("Authors:", ", ".join(item['Authors']))
    print("Abstract:", item['Abstract'])
    print("---")

# If needed, you could save 'data' to a
import pandas as pd

# Convert list of dictionaries to DataFrame
df = pd.DataFrame(data)

# Save to CSV
df.to_csv('arxiv_articles.csv', index=False)


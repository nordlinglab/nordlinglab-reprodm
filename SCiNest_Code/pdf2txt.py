import PyPDF2

def pdf2txt(file_path):
    with open(file_path, 'rb') as file:
        pdf_reader = PyPDF2.PdfReader(file)

        full_text = []

        for page in pdf_reader.pages:
            full_text.append(page.extract_text())
        return "\n".join(filter(None, full_text))
    
file = input("Input the path to your file: ")
text_output = pdf2txt(file)
print(text_output)
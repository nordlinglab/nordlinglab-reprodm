import pandas as pd

first = pd.read_csv("arxiv_articles_1.csv")
second = pd.read_csv("arxiv_articles_2.csv")
third = pd.read_csv("arxiv_articles_3.csv")

merged = pd.concat([first, second, third])
merged.to_csv("merged_arxiv_articles.csv")

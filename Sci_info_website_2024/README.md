# Sci_info_website: Reproducibility Assessment of Academic Papers

## Description
Sci_info is developed by eight graduate students from NCKU in Professor Nordling's course "Scientific Information & Gathering (Sci Info)". 
The team members include:
- David Hernandez
- Rex Lee
- Louis 
- Dieter
- Joey Wang
- Henry
- Sibonginkhosi Nkambule
- Prashant Kishor Sharma

Our tool help you retrieve relevant articles faster in your literature review and allows users to upload or input the full text of scientific articles and returns a reproducibility score. 
This is powered by using the Llama3 model via Ollama.

# File Structure
**Project Directory**
```
Sci_info_website_2024
│   README.md 
│   requirements.txt  # Contains all dependencies required for the project
│   utils.py # llama3 function
│   reprod_score.py # main file
│
└───templates
│   │   response.html # front end of the website
│   │   list_paper # List of all articles from Scopus
```

## Installation

### Prerequisites
- Python 3.10 +
- Conda (recommended for environment management)

### Scopus API retrieval
1. Go to [Elsevier](https://dev.elsevier.com/) to get the API.
2. Sign in to Scopus account and get the API key.
3. Click the create API button.
4. Type a name for the label.
5. Read and argee the policy.
6. Click the summit button.

note: Save the API key, it will be use later.

### Setting up the environment (optional but recommended)
1. Install Conda, if not already installed, from [Miniconda](https://docs.conda.io/en/latest/miniconda.html).

2. Create a new Conda environment in :
- If on **Windows**, open "*Anaconda Prompt*".
- If on **Linux** or **Mac**, open "*Terminal*"

   ```bash
   conda create -n your_env_name python=3.11
   ```
3. Activate the environment:
   ```bash
   conda activate your_env_name
   ```

### Setting up Ollama
1. Install Ollama, if not already installed, from [Ollama](https://ollama.com/download).
2. Download Llama3 by running:
   ```bash
   ollama run llama3
   ```

### Installing dependences
- If on **Windows**, open "*Anaconda Prompt*".
- If on **Linux** or **Mac**, open "*Terminal*"

Ensure you are in the correct:
- project directory by running:
   ```bash
   cd path/to/your/project/directory
   ```
- conda environment by running: (if yor using conda)
   ```bash
   conda activate your_env_name
   ```


Then run:
   ```bash
   pip install -r requirements.txt
   ```

# Usage
To start the application, run:
   ```bash
   python reprod_score.py
   ```
After running the script, open a web browser and navigate to:
   ```bash
   http://127.0.0.1:5000
   ```
## Note (First time usage)
First time running this code, go to Search Papers page, enter Scopus type search string (It should be sensitive)search string, and click submit.

Enter the Scopus API key in the terminal and press ENTER twice. And it can work.



# Server Setup Guide for Linux (Local)

## Create Environment
To create a virtual environment, execute:
```sh
python3 -m venv ~/env/yourenvironmentname
source ~/env/yourenvironmentname/bin/activate
```

## Install Required Packages
First, ensure your package lists are up-to-date and then install vim:
```sh
sudo apt-get install vim
```
Then, install the required Python packages:
```sh
pip install -r requirements.txt
```

## Test Reprod_Score
Run the script to test:
```sh
python3 reprod_score.py
```

## Activate Gunicorn
First, edit the WSGI entry point:
Note: If you don't have permissions add sudo before vim.
```sh
vim wsgi.py
```
In the Python file, ensure you have the content below:
```python
if __name__ == "__main__":
    app.run()
```
After type in the terminal 
```
:wq
```
In vim, w means write, and q means quit.

Then, start Gunicorn in the terminal with:
```sh
gunicorn --bind 0.0.0.0:9090 wsgi:app
```

## Quit Environment
To deactivate the virtual environment:
```sh
deactivate
```

## SystemD Configuration
Create a new service file using:
```sh
sudo vim /etc/systemd/system/reprod_score.service
```
Ensure the service file content is:
```
[Unit]
Description=Gunicorn instance to serve reprod_score Flask app
After=network.target

[Service]
User=nordlinglab
Group=www-data
WorkingDirectory=/home/nordlinglab/Documents/Repositories/nordlinglab-reprodm/Sci_info_website_2024
Environment="PATH=/home/nordlinglab/env/web/bin"
ExecStart=/home/nordlinglab/env/web/bin/gunicorn --workers 3 --bind unix:/home/nordlinglab/Documents/Repositories/nordlinglab-reprodm/Sci_info_website_2024/reprod_score.sock -m 007 wsgi:app

[Install]
WantedBy=multi-user.target
```
Start, reload, enable, and check the status of your service:
```sh
sudo systemctl start reprod_score
sudo systemctl daemon-reload
sudo systemctl enable reprod_score
sudo systemctl status reprod_score
```

## Install Nginx
Install Nginx using:
```sh
sudo apt install nginx
```

## Configure Nginx
Create and edit the Nginx server block:
```sh
sudo vim /etc/nginx/sites-available/reprod_score.conf
```
Ensure the server block content is:
Note: Make sure to change the path accordingly.
```
server {
    listen 80;
    server_name sciinfo.onthewifi.com www.sciinfo.onthewifi.com;

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/nordlinglab/Documents/Repositories/nordlinglab-reprodm/Sci_info_website_2024/reprod_score.sock;
    }
}

access_log /var/log/nginx/reprod_score_access.log;
error_log /var/log/nginx/reprod_score_error.log;
```

## Create Symbolic Link
Create a symbolic link to enable the Nginx site:
```sh
sudo ln -s /etc/nginx/sites-available/reprod_score.conf /etc/nginx/sites-enabled/
sudo nginx -t
sudo systemctl restart nginx
```

## Firewall Configuration
Check the status and allow Nginx through the firewall:
```sh
sudo ufw status
sudo ufw allow "Nginx Full"
```

## Additional Information

### Virtual Environment Management
- **Activate the environment**: `source ~/env/yourenvironmentname/bin/activate`
- **Deactivate the environment**: `deactivate`
- **Delete the environment**: Remove the folder `~/env/yourenvironmentname`

### Common Gunicorn Commands
- **Restart Gunicorn**: `sudo systemctl restart reprod_score`
- **Stop Gunicorn**: `sudo systemctl stop reprod_score`
- **Check Gunicorn Logs**: `sudo journalctl -u reprod_score`

### Common Nginx Commands
- **Test Nginx Configuration**: `sudo nginx -t`
- **Restart Nginx**: `sudo systemctl restart nginx`
- **Check Nginx Status**: `sudo systemctl status nginx`

### Troubleshooting Tips
- **Gunicorn Errors**: Check the Gunicorn logs for errors: `sudo journalctl -u reprod_score`
- **Nginx Errors**: Check the Nginx logs for errors: `cat /var/log/nginx/reprod_score_error.log`


# License
This project is licensed under Apache License 2.0

# Contact
For further assistance, contact:
- David Hernandez: david.hernandez@nordlinglab.org
- Rex Lee: rex.lee@nordlinglab.org
- Louis: louis.chen@nordlinglab.org
- Dieter Rahmadiawan: dieterrahmadiawan@gmail.com
- Joey Wang: joey.wang@nordlinglab.org
- Henry: henrysemailJ@gmail.com
- Sibonginkhosi: sibo.nkambule@nordlinglab.org
- Prashant Kishor Sharma: prashant94580@gmail.com

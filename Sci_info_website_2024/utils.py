import ollama

def analysis(model='llama3', text='', prompt='Hello'):
    response = ollama.generate(model=model,
            prompt= prompt + text + "Give me a score from 0-10.")
    analysis_result = response['response']
    return analysis_result
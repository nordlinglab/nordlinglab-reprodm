import os
import traceback
import pybliometrics
# from pybliometrics.scopus import scopus_search, abstract_retrieval
from pybliometrics.scopus import ScopusSearch, AbstractRetrieval
import json
from tqdm import tqdm
import shutil


def find_article(search_string, year1, year2):
    folder_all_articles_path = 'pybliometrics_ml/output'
    folder_name = "output"

    # 使用 os.makedirs 創建資料夾
    os.makedirs(folder_name, exist_ok=True)

    try:
        shutil.rmtree(folder_all_articles_path)
        print(f"folder '{folder_all_articles_path}' has already deleted。")
    except Exception as e:
        print(f"Can't not delete the folder: {e}")

    # NOTE: config file for pybliometrics is stored in $HOME/.config/pybliometrics.cfg

    pybliometrics.scopus.init()
    for year in range(year1, year2+1):
        # make the folder to store the data for the year
        current_path = os.getcwd()
        print(current_path)
        folder_path = os.path.join(current_path, f'{folder_all_articles_path}', str(year))
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
        # web crawler
        # get the results
        x = ScopusSearch(search_string, view="STANDARD")
        print(f"Year: {year} , Results count: {len(x.results)}")

        # store the results and add the ref_docs key to store each reference
        for doc in tqdm(x.results):
            try:
                # store each result in a file labeled by its Scopus EID
                doc_dict = doc._asdict()
                eid = doc_dict["eid"]
                doi = doc_dict["doi"]
                file_path = os.path.join(folder_path, f"{eid}.json")
                if not os.path.exists(file_path):
                    # Look up the references / citations for that document
                    document = AbstractRetrieval(eid, view="REF")
                    articles = AbstractRetrieval(f"{doi}")
                    abstract = articles.description
                    doc_dict["abstract"] = abstract
                    refs = []
                    # Store the references
                    for ref in document.references:
                        ref_doc = {"doi": ref.doi, "title": ref.title,
                                   "id": ref.id, "sourcetitle": ref.sourcetitle}
                        refs.append(ref_doc)
                    doc_dict["ref_docs"] = refs
                    # Dump the dictionary to the JSON file
                    with open(file_path, "w") as json_file:
                        json.dump(doc_dict, json_file)
                else:
                    print("SKIP (File already exists)")

            # we're not going to try too hard to fix any of the rare errors
            except Exception as e:
                pass
                # print(f"An error occurred: {e}")
                # traceback.print_exc()

        directory = f'{folder_all_articles_path}/{year}'

        save_path = f'{folder_all_articles_path}/merged_data'
        save_filename = 'merged_file.json'
        full_save_path = os.path.join(save_path, save_filename)

        if not os.path.exists(save_path):
            os.makedirs(save_path)

        merged_data = []

        for filename in os.listdir(directory):
            if filename.endswith('.json'):
                filepath = os.path.join(directory, filename)
                with open(filepath, 'r', encoding='utf-8') as file:
                    data = json.load(file)
                    merged_data.append(data)

        merged_data.reverse()

        with open(full_save_path, 'w', encoding='utf-8') as file:
            json.dump(merged_data, file, ensure_ascii=False, indent=4)

    print(f"merged！")
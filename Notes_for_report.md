# Notes for Technical Report 2022-03-25 #

## PRISMA Flowchart ##

Make a new PRISMA flowchart for Q1 using team SpinBec structure. 

* Include all databases used.
* List reasons for article exclusion with bulletpoints.
* Make the flowchart more visually appealing and organized.

Also make PRISMA flowchart for Q2 and Q3

### On databsases ###

* Google scholar for more general search
* Web of Science and Scopus for journals only. (Web of Science more distinguished)
* Scopus has some things that web of science doesn’t have
* Science direct restricts you to Elsevier published articles
* EBSCO: different libraries have different material

## Search string team ##

Make a sensitive search string (covers all topics), then gradually make it more specific.
* OR --> more general
* AND and quotes "" --> more specific

Some tips:

* (computational AND reproducibility) --> “computational reproducibility”
* (science AND reproducibility)--> “reproducible science”
* Add the term “computational” as stand alone

## Q2 Articles on tools to asses reproducibility ##

The articles found listed methods to **improve** reproducibility *not* **assess** it

* Include Yang and prediction markets are tools to **assess** reproducibility
* Add list of tools to **improve** reproducibility
* Add list of tools to **assess** reproducibility


## Q3 Articles investigating reproducibility of scientific work ##
Some of the articles found refer to reproducibility in a very specific field or implementation. We are interested in studies that investigate if a study or group studies themselves can be reproduced. 


## Q4 webservices ##
* Sci-score
* Protocols.io (questionaire)

## Q5 ##

None of the patents chosen are relevant to assessment of reproducibility. The search strings are probably not working, too many hits. 
Expected at most a few handfull of patents if any.

## Q6 ##

We are looking for an article that does a meta-analysis of articles discussing reproducibility of science.
To find review article in google scholar add: review, overview, meta-analysis in the to the search string




